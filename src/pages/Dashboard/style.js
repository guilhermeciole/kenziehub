import styled, { keyframes } from "styled-components";

const KeyFrameImg = keyframes`
    from {
        opacity: 0;
        transform: translateX(400px)
    }

    to {
        opacity: 1;
        transform: translateX(0px);
    }
`;

export const ContainerSubmit = styled.div`
  display: flex;
  flex-direction: row;
  margin-bottom: 50px;

  input {
    margin-right: 50px;
  }

  button {
    margin-top: 10px;
    margin-left: 10px;
  }
`;

export const Image = styled.img`
  position: absolute;
  right: 30px;
  top: 200px;
  animation: ${KeyFrameImg} 1.5s;
`;

export const ContainerButtonQuit = styled.div`
  position: absolute;
  bottom: 30px;
  right: 100px;
`;
