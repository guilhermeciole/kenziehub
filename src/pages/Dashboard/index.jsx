import { Button, TextField } from "@material-ui/core";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { Redirect } from "react-router-dom";
import { toast } from "react-toastify";
import { Bar } from "../../components/Bar";
import { ButtonSubmit } from "../../components/Button";
import { api } from "../../services/api";
import { ContainerButtonQuit, ContainerSubmit, Image } from "./style";
import image from "../../assets/imageDashboard.svg";
import { useHistory } from "react-router";

export const Dashboard = ({ authenticated }) => {
  const [tech, setTech] = useState([]);
  const [token, setToken] = useState(
    JSON.parse(localStorage.getItem("@KenzieHub:token")) || ""
  );

  const history = useHistory();
  const { register, handleSubmit } = useForm();

  const handleForm = (data) => {
    api
      .post("/users/techs", data, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        setTech([...tech, res.data]);
        toast("Tecnologia adicionada!");
      })
      .catch(() => toast.error("Tecnologia já exite!"));
  };

  const handleDelete = (id) => {
    const newTech = tech.filter((item) => item.id !== id);

    api
      .delete(`/users/techs/${id}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((_) => {
        setTech(newTech);
        toast.dark("Tecnologia excluída!");
      });
  };

  const handleQuit = () => {
    setToken(localStorage.clear("@KenzieHub:token", JSON.stringify(token)));
    window.location.reload();
  };

  if (!authenticated) {
    return <Redirect to="/" />;
  }

  return (
    <>
      <h2>Adicionar tecnologias</h2>
      <form onSubmit={handleSubmit(handleForm)}>
        <ContainerSubmit>
          <TextField
            ize="small"
            variant="outlined"
            label="Título"
            margin="dense"
            {...register("title")}
          />
          <TextField
            ize="small"
            variant="outlined"
            label="Status"
            margin="dense"
            {...register("status")}
          />
          <ButtonSubmit>Adicionar</ButtonSubmit>
        </ContainerSubmit>
      </form>

      <div>
        {tech.map((item) => (
          <Bar
            item={item}
            key={item.id}
            onClick={() => handleDelete(item.id)}
          />
        ))}
      </div>
      <Image src={image} alt="Imagem área de trabalho" width="600" />
      <ContainerButtonQuit>
        <Button onClick={handleQuit}>Sair</Button>
      </ContainerButtonQuit>
    </>
  );
};
