import { TextField } from "@material-ui/core";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import { ButtonSubmit } from "../../components/Button";
import { useHistory } from "react-router";
import { api } from "../../services/api";
import { toast } from "react-toastify";
import { Link, Redirect } from "react-router-dom";
import { Container, Img, P } from "./style";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Image from "../../assets/imageLogin.svg";

export const Login = ({ authenticated, setAuthenticated }) => {
  const history = useHistory();

  const formSchema = yup.object().shape({
    email: yup.string().required("Email obrigatório").email("Email inválido"),
    password: yup
      .string()
      .required("Senha obrigatória")
      .matches(
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/,
        "Mínimo de 8 caracters, letra maiúscula, letra minúscula, número e caractere especial"
      ),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(formSchema),
  });

  const handleForm = (data) => {
    api
      .post("/sessions", data)
      .then((res) => {
        const { token } = res.data;
        localStorage.setItem("@KenzieHub:token", JSON.stringify(token));
        toast.info("Bem vindo!");
        setAuthenticated(true);
        return history.push("/dashboard");
      })
      .catch((_) => toast.error("Email ou senha incorretos"));
  };

  const useStyles = makeStyles((theme) => ({
    root: {
      display: "flex",
      flexWrap: "wrap",
      "& > *": {
        margin: theme.spacing(1),
        width: theme.spacing(35),
        height: theme.spacing(40),
        padding: theme.spacing(3),
      },
    },
  }));

  const classes = useStyles();

  if (authenticated) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <>
      <Container>
        <div className={classes.root}>
          <Paper>
            <h2>Entrar</h2>
            <form onSubmit={handleSubmit(handleForm)}>
              <div>
                <TextField
                  required
                  ize="small"
                  variant="outlined"
                  label="Email"
                  margin="dense"
                  {...register("email")}
                  error={!!errors.email}
                  helperText={errors.email?.message}
                />
              </div>
              <div>
                <TextField
                  required
                  ize="small"
                  variant="outlined"
                  label="Senha"
                  margin="dense"
                  {...register("password")}
                  error={!!errors.password}
                  helperText={errors.password?.message}
                />
              </div>
              <ButtonSubmit>Enviar</ButtonSubmit>
            </form>
            <P>
              Não possui conta?<Link to="/register">Cadastre-se</Link>!
            </P>
          </Paper>
        </div>
      </Container>
      <Img src={Image} alt="Imagem de login" width="600" />
    </>
  );
};
