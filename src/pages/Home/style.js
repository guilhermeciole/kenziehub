import styled from "styled-components";

export const LogoBox = styled.div`
  position: absolute;
  left: 50px;
  top: 40px
`;

export const Titulo = styled.h1`
  font-size: 120px;
  margin-top: 100px;
  text-shadow: 3px 5px 2px #474747;
`;

export const Span = styled.span`
  font-weight: bolder;
  color: var(--blueKenzie);
`;

export const DivButton = styled.div`
  margin-top: 60px;
`;
