import { Button } from "@material-ui/core";
import { DivButton, Logo, LogoBox, Span, Titulo } from "./style";
import { useHistory } from "react-router";
import { Redirect } from "react-router-dom";
import logo from "../../assets/logoKenzie.svg";

export const Home = ({ authenticated }) => {
  const history = useHistory();

  const handleClick = () => history.push("/login");

  if (authenticated) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <>
      <LogoBox>
        <img src={logo} alt="logoKenzie" width="50"></img>
      </LogoBox>
      <Titulo>KenzieHub</Titulo>
      <p>
        Um hub de portfólios de programadores da <Span>Kenzie</Span>
      </p>
      <DivButton>
        <Button variant="outlined" color="primary" onClick={handleClick}>
          Login
        </Button>
      </DivButton>
    </>
  );
};
