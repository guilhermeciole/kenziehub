import styled, { keyframes } from "styled-components";

const KeyFrameForm = keyframes`
    from {
        opacity: 0;
        transform: translateX(-200px)
    }

    to {
        opacity: 1;
        transform: translateX(0px);
    }
`;

const KeyFrameImg = keyframes`
    from {
        opacity: 0;
        transform: translateY(400px)
    }

    to {
        opacity: 1;
        transform: translateY(0px);
    }
`;

export const Container = styled.div`
  position: absolute;
  left: 200px;
  top: 50px;
  animation: ${KeyFrameForm} 1.5s;
`;

export const P = styled.p`
  font-size: 15px;
  margin-top: 9px;
`;

export const ContainerImg = styled.div`
  position: absolute;
  right: 90px;
  top: 100px;
  animation: ${KeyFrameImg} 1.5s;
`;
