import { TextField } from "@material-ui/core";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import { ButtonSubmit } from "../../components/Button";
import { useHistory } from "react-router";
import { api } from "../../services/api";
import { toast } from "react-toastify";
import { Redirect } from "react-router-dom";
import { Link } from "react-router-dom";
import { Container, ContainerImg, P } from "./style";
import image from "../../assets/imageRegister.svg";
import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";

export const Register = ({ authenticated }) => {
  const history = useHistory();

  const formSchema = yup.object().shape({
    email: yup.string().required("Email obrigatório").email("Email inválido"),
    password: yup
      .string()
      .required("Senha obrigatória")
      .matches(
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/,
        "Mínimo de 8 caracters, letra maiúscula, letra minúscula, número e caractere especial"
      ),
    name: yup
      .string()
      .required("Usuário obrigatório")
      .matches(/^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ ]+$/, "Apenas letras"),
    bio: yup.string().required("Bio é obrigatório"),
    contact: yup.string().required("Contato obrigatório"),
    course_module: yup.string().required("Módulo do curso obrigatório"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(formSchema),
  });

  const handleForm = (data) => {
    api
      .post("/users", data)
      .then((_) => {
        toast.success("Sucesso no cadastro!");
        history.push("/login");
      })
      .catch((_) => toast.error("Email já casdastrado!"));
  };

  const useStyles = makeStyles((theme) => ({
    root: {
      display: "flex",
      flexWrap: "wrap",
      "& > *": {
        margin: theme.spacing(1),
        width: theme.spacing(35),
        height: theme.spacing(66),
        padding: theme.spacing(3),
      },
    },
  }));

  const classes = useStyles();

  if (authenticated) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <>
      <Container>
        <div className={classes.root}>
          <Paper>
            <h2>Cadastro</h2>
            <form onSubmit={handleSubmit(handleForm)}>
              <div>
                <TextField
                  required
                  ize="small"
                  variant="outlined"
                  label="Email"
                  margin="dense"
                  {...register("email")}
                  error={!!errors.email}
                  helperText={errors.email?.message}
                />
              </div>
              <div>
                <TextField
                  required
                  ize="small"
                  variant="outlined"
                  label="Senha"
                  margin="dense"
                  {...register("password")}
                  error={!!errors.password}
                  helperText={errors.password?.message}
                />
              </div>
              <div>
                <TextField
                  required
                  ize="small"
                  variant="outlined"
                  label="Nome"
                  margin="dense"
                  {...register("name")}
                  error={!!errors.name}
                  helperText={errors.name?.message}
                />
              </div>
              <div>
                <TextField
                  required
                  ize="small"
                  variant="outlined"
                  label="Bio"
                  margin="dense"
                  {...register("bio")}
                  error={!!errors.bio}
                  helperText={errors.bio?.message}
                />
              </div>
              <div>
                <TextField
                  required
                  ize="small"
                  variant="outlined"
                  label="Contato"
                  margin="dense"
                  {...register("contact")}
                  error={!!errors.contact}
                  helperText={errors.contact?.message}
                />
              </div>
              <div>
                <TextField
                  required
                  ize="small"
                  variant="outlined"
                  label="Módulo do curso"
                  margin="dense"
                  {...register("course_module")}
                  error={!!errors.course_module}
                  helperText={errors.course_module?.message}
                />
              </div>
              <ButtonSubmit>Enviar</ButtonSubmit>
            </form>
            <P>
              Já tem conta? <Link to="/login">Entrar</Link>!
            </P>
          </Paper>
        </div>
      </Container>

      <ContainerImg>
        <img src={image} alt="Register" width="600" />
      </ContainerImg>
    </>
  );
};
