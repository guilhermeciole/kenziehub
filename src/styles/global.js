import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`

    * {
        outline: 0;
        box-sizing: border-box;
        margin: 0;
        padding: 0;
    }

    :root {
        --black: #000000;
        --blueKenzie: #007AFF;
        --body: #D9E1FB;
    }

    body {
        background: var(--body);
    }

    h1 {
        font-family: 'Alfa Slab One', cursive;
        font-weight: 100;
        color: var(--blueKenzie);
    }

    h2, h3, h4, h5, h6 {
        font-family: 'Righteous', cursive;
        font-weight: 100;
        color: var(--blueKenzie);
        }

    p {
        font-family: 'Roboto', sans-serif;
        font-weight: 300;
        color: var(--black);
    }

    a {
        color: var(--blueKenzie);
        font-weight: bolder;
        text-decoration: none;
    }


    
`;
