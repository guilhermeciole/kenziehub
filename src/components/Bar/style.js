import styled, { keyframes } from "styled-components";

const KeyFrames = keyframes`
    from {
        opacity: 0;
        transform: translateY(-400px)
    }

    to {
        opacity: 1;
        transform: translateY(0px);
    }
`;

export const Bars = styled.div`
  margin-right: 600px;
  animation: ${KeyFrames} 1s;
`;
export const Container = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 10px;

  p {
    font-size: 18px;
    padding-top: 7px;
  }
`;

export const Span = styled.span`
  font-weight: bold;
  color: var(--black);
  font-size: 18px;
`;
