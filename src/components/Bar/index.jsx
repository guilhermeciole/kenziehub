import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import { Button } from "@material-ui/core";
import { Bars, Container, Span } from "./style";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    "& > *": {
      margin: theme.spacing(1),
      width: theme.spacing(70),
      height: theme.spacing(7),
    },
  },
}));

export const Bar = ({ item, onClick }) => {
  const classes = useStyles();

  return (
    <Bars className={classes.root}>
      <Paper elevation={4}>
        <Container>
          <p>
            <Span>{item.title}</Span>: {item.status}
          </p>
          <Button onClick={onClick}>Deletar</Button>
        </Container>
      </Paper>
    </Bars>
  );
};
