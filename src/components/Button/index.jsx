import { Button } from "@material-ui/core";

export const ButtonSubmit = ({ children }) => {
  return (
    <div>
      <Button type="submit" variant="contained" color="primary">
        {children}
      </Button>
    </div>
  );
};
